<?php

namespace FirewallService\Interfaces;

interface ProviderInterface
{
    public function install(): bool;
    public function enable(bool $value): void;
    public function allow(string $ip): bool;
    public function deny(string $ip): bool;
}
