<?php

namespace FirewallService\Providers;

use FirewallService\Interfaces\ProviderInterface;

class Nginx extends Base implements ProviderInterface
{
    protected $active = true;
    protected $pathToBlackList = "/etc/nginx/firewall_blacklist.conf";

    public function install(): bool
    {
        if(file_exists($this->pathToBlackList)){
            return true;
        }

        $result = copy(__DIR__."/../../config/firewall_blacklist.conf", $this->pathToBlackList);

        if($result){
            $this->execCommand("chown www-data:www-data ".$this->pathToBlackList." && chmod 0777 ".$this->pathToBlackList);
        }

        return $result;
    }

    public function allow(string $ip): bool
    {
        return false;
    }

    public function deny(string $ip): bool
    {
        $result = false;

        $output = file_put_contents($this->pathToBlackList,"deny {$ip};", FILE_APPEND);

        if($output !== false){

            $result = true;

            $this->reload();
        }

        return $result;
    }

    private function reload(){
        $this->execCommand("/etc/init.d/nginx reload");
    }
}
