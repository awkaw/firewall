<?php

namespace FirewallService\Providers;

use FirewallService\Interfaces\ProviderInterface;

class Ufw extends Base implements ProviderInterface
{
    protected $active = false;

    public function allow(string $ip): bool
    {
        //$result = $this->execCommand("ufw allow {$ip} to any port");

        return false;
    }

    public function deny(string $ip): bool
    {
        $result = false;

        $output = $this->execCommand("ufw deny from {$ip}");

        if(is_array($output) && !empty($output)){

            foreach ($output as $item) {

                if(preg_match('#Rule added#', $item)){
                    $result = true;
                }
            }
        }

        return $result;
    }
}
