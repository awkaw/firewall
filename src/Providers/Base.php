<?php


namespace FirewallService\Providers;


use FirewallService\Interfaces\ProviderInterface;

class Base implements ProviderInterface
{
    protected $active = false;

    public function enable(bool $value): void
    {
        $this->active = $value;
    }

    public function allow(string $ip): bool
    {
        return false;
    }

    public function deny(string $ip): bool
    {
        return false;
    }

    protected function execCommand(string $command): array{

        exec($command, $output);

        return $output;
    }

    protected function getCurrentSystemUser(){
        return posix_getpwuid(posix_geteuid());
    }

    public function install(): bool
    {
        return false;
    }
}
