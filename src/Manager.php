<?php


class Manager
{
    private static $instance;
    private $providers = [];

    public function install(){

        $providers = $this->getListProviders();

        foreach ($providers as $provider) {

            $providerObj = new $provider;

            if($providerObj instanceof \FirewallService\Interfaces\ProviderInterface){
                $providerObj->install();
            }
        }
    }

    public static function getInstance(): Manager{

        if(!self::$instance instanceof Manager){
            self::$instance = new Manager();
        }

        return self::$instance;
    }

    public function getListProviders(){

        return [
            \FirewallService\Providers\Nginx::class,
            \FirewallService\Providers\Ufw::class,
        ];
    }

    public function init(){

        if(empty($this->providers)){

            $providers = $this->getListProviders();

            foreach ($providers as $provider) {

                $providerObj = new $provider;

                if($providerObj instanceof \FirewallService\Interfaces\ProviderInterface){

                    $this->providers[] = $providerObj;
                }
            }
        }
    }

    public function allow(string $ip){

        $this->init();

        foreach ($this->providers as $provider) {

            if($provider instanceof \FirewallService\Interfaces\ProviderInterface){
                $provider->allow($ip);
            }
        }
    }

    public function deny(string $ip){

        $this->init();

        foreach ($this->providers as $provider) {

            if($provider instanceof \FirewallService\Interfaces\ProviderInterface){
                $provider->deny($ip);
            }
        }
    }
}
