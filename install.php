<?php

require_once __DIR__."/src/Manager.php";
require_once __DIR__."/src/Interfaces/ProviderInterface.php";
require_once __DIR__."/src/Providers/Base.php";
require_once __DIR__."/src/Providers/Nginx.php";
require_once __DIR__."/src/Providers/Ufw.php";

$manager = Manager::getInstance();

$manager->install();
